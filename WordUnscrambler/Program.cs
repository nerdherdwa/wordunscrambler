﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordUnscrambler.Workers;
using WordUnscrambler.Data;


namespace WordUnscrambler
{
    class Program
    {       
        private static readonly FileReader _fileReader = new FileReader();
        private static readonly WordMatcher _wordMatcher = new WordMatcher();

        static void Main(string[] args)
        {
            try
            {
                bool continueWordUnscramble = true;
                do
                {
                    Console.WriteLine(Constants.OptionsUserInput);
                    var option = Console.ReadLine() ?? string.Empty;
                    Console.Clear();

                    switch (option.ToUpper())
                    {
                        // determine file type
                        case Constants.File:
                            Console.Write(Constants.EnterFilePathName);
                            ExecuteScrambledFile();
                            break;
                        case Constants.Manual:
                            Console.Write(Constants.EnterManually);
                            ExecuteScrambledManual();
                            break;
                        default:
                            Console.WriteLine(Constants.EnterOptionUnrecongised);
                            Console.WriteLine();
                            break;
                    }

                    var continueDecision = string.Empty;
                    do
                    {
                        Console.Write(Constants.OptionsContinueMessage);
                        continueDecision = (Console.ReadLine() ?? string.Empty);
                        
                    } while (
                            !continueDecision.Equals(Constants.Yes, StringComparison.OrdinalIgnoreCase) &&
                            !continueDecision.Equals(Constants.No, StringComparison.OrdinalIgnoreCase));

                    continueWordUnscramble = continueDecision.Equals(Constants.Yes, StringComparison.OrdinalIgnoreCase);
                    Console.Clear();

                } while (continueWordUnscramble);
            }
            catch (Exception ex)
            {
                Console.WriteLine(Constants.ErrorTerminate + ex.Message);
            }      
        }

        private static void ExecuteScrambledManual()
        {
            var manualInput = Console.ReadLine() ?? string.Empty;
            string[] scrambledWords = manualInput.Split(',');
            DisplayMatch(scrambledWords);
        }

        private static void ExecuteScrambledFile()
        {
            try
            {
                var filename = Console.ReadLine() ?? string.Empty;
                string[] scrambledWords = _fileReader.Read(filename);
                DisplayMatch(scrambledWords);
            }
            catch (Exception ex)
            {
                Console.WriteLine(Constants.ErrorLoaded + ex.Message);
            }

        }
        private static void DisplayMatch(string[] scrambledWords)
        {
            string[] wordList = _fileReader.Read(Constants.wordListFileName);

            List<MatchedWord> matchedWords = _wordMatcher.Match(scrambledWords, wordList);
            
            if(matchedWords.Any())
            {
                foreach (var matchedWord in matchedWords)
                {
                    Console.WriteLine(Constants.MatchFound, matchedWord.ScrambledWord, matchedWord.Word);
                }
            }
            else
            {
                Console.WriteLine(Constants.MatchNotFound);
            }
        }
    }
}
